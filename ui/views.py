from django.shortcuts import render
import requests
from django.core.serializers.json import DjangoJSONEncoder
from django.utils.encoding import force_text
from django.core.serializers.json import DjangoJSONEncoder
from rest_framework.exceptions import NotFound
from rest_framework import generics
from django.contrib.auth.models import User
from django.core.serializers import serialize
from django.http import HttpResponseRedirect, Http404
from .forms import TasklistCreateForm, TaskCreateForm, TasklistDetailsForm, TaskDetailsForm, TagCreateForm, RegisterForm, LoginForm, ConfirmationForm
from django.contrib.sessions.models import Session
from django.contrib import auth
from django.core.mail import send_mail
from django import forms

#def get_tasklist(request, list_id):
#    response = requests.get('http://127.0.0.1:8000/todolists/', headers=dict(Authorization=request.session['Authorization'])).json()
#    lists = []
#    for i in response:
#        lists.append((i['id'], i['name']))
#    return tuple(lists)

def get_tasks(request):
    response = requests.get('http://127.0.0.1:8000/todolists/tasks/', headers=dict(Authorization=request.session['Authorization'])).json()
    tasks = [(i, response[i]['name']) for i in range(len(response))]
    return tasks

def get_tags():
    response = requests.get('http://127.0.0.1:8000/todolists/tags').json()
    #tags = [i['tag'] for i in response]
    return response

def create_tag(request, tag):
    print('tag ' + tag)
    print(request.session['Authorization'])
    requests.Session().post('http://127.0.0.1:8000/todolists/tags/',
            data = dict(tag = tag),
            headers = dict(Authorization=request.session['Authorization'])
            )

def get_friends(request):
    response = requests.get('http://127.0.0.1:8000/todolists/shared/', headers=dict(Authorization=request.session['Authorization'])).json()
    friends = [(i, response[i]['username']) for i in range(len(response))]
    return friends

def get_users(request):
    response = requests.get('http://127.0.0.1:8000/todolists/registration/', headers=dict(Authorization=request.session['Authorization'])).json()
    users = [(i, response[i]['username']) for i in range(len(response))]
    return users

def tasklist_create(request):
    try:
        a = request.session['Authorization']
    except:
        return HttpResponseRedirect('http://localhost:8080/ui/login/')
    if request.method == 'POST':
        form = TasklistCreateForm(request.POST)
        if form.is_valid():
            requests.Session().post('http://127.0.0.1:8000/todolists/',
                data = dict(name = form.cleaned_data['name'],
                            friends = form.cleaned_data['friends']),
                headers = dict(Authorization=a))
    else:
        form = TasklistCreateForm()
        form.fields['friends'].choices = get_users(request)
    resp = requests.get('http://127.0.0.1:8000/todolists/',
                            headers = dict(Authorization=a)).json()
    resp += requests.get('http://127.0.0.1:8000/todolists/shared',
                            headers = dict(Authorization=a)).json()
    return render(request, 'tasklistcreatetpl.html', {'form': form, 'resp': resp})

def task_create(request, list_id):
    try:
        a = request.session['Authorization']
    except:
        return HttpResponseRedirect('http://localhost:8080/ui/login/')
    all_tags = get_tags() 
    tag_dict = {}
    for tag in all_tags: 
        tag_dict[tag['tag']] = tag['id']
    if request.method == 'POST':
        form = TaskCreateForm(request.POST)
        if form.is_valid():
            tags = form.cleaned_data['tags']
            tags = tags.split()
            print(tags)
            wereNew = False 
            for tag in tags:
                if tag not in tag_dict.keys():
                    wereNew = True 
                    create_tag(request, tag)
            #print(dict(name = form.cleaned_data['name'],
            #                description = form.cleaned_data['description'],
            #                completed = form.cleaned_data['completed'],
            #                #date_created = dd.today(),
            #                due_date = form.cleaned_data['due_date'],
            #                #date_modified = dd.today(),
            #                tags = tag_response,
            #                priority = form.cleaned_data['priority']))
            requests.Session().post('http://127.0.0.1:8000/todolists/' + str(list_id) + '/tasks/', 
                            data = dict(name = form.cleaned_data['name'],
                            description = form.cleaned_data['description'],
                            completed = form.cleaned_data['completed'],
                            #date_created = dd.today(),
                            due_date = form.cleaned_data['due_date'],
                            #date_modified = dd.today(),
                            #tags = tag_response,
                            tags = form.cleaned_data['tags'],
                            priority = form.cleaned_data['priority']),
                            headers = dict(Authorization=a))
            #tasklist = form.cleaned_data['tasklist']
    else:
        form = TaskCreateForm()
    resp = requests.get('http://127.0.0.1:8000/todolists/' + str(list_id) + '/tasks/',
                        headers = dict(Authorization=a)).json()
        #form.fields['tasklist'].choices = get_tasklists(request)
    return render(request, 'taskcreatetpl.html', {'form': form, 'action': request.path, 'resp': resp})

def task_details(request, list_id, pk):
        try:
            a = request.session['Authorization']
        except:
            return HttpResponseRedirect('http://localhost:8080/ui/login/')
        all_tags = get_tags() 
        tag_dict = {}
        for tag in all_tags: 
            tag_dict[tag['tag']] = tag['id']
        if request.method == 'POST':
            form = TaskDetailsForm(request.POST)
            if form.is_valid():
                method = form.cleaned_data['method']
            if method == 'put':
                if form.is_valid():
                    tags = form.cleaned_data['tags']
                    tags = tags.split()
                    print(tags)
                    wereNew = False 
                    for tag in tags:
                        if tag not in tag_dict.keys():
                            wereNew = True 
                    create_tag(request, tag)
                    requests.Session().put('http://127.0.0.1:8000/todolists/' + str(list_id) + '/tasks/' + str(pk), 
                                data = dict(name = form.cleaned_data['name'],
                                description = form.cleaned_data['description'],
                                completed = form.cleaned_data['completed'],
                                #date_created = dd.today(),
                                due_date = form.cleaned_data['due_date'],
                                #date_modified = dd.today(),
                                tags = form.cleaned_data['tags'],
                                priority = form.cleaned_data['priority']),
                                headers = dict(Authorization=a))
            else:
                requests.Session().delete('http://127.0.0.1:8000/todolists/' + str(list_id) + '/tasks' + str(pk) + '/',
                            headers = dict(Authorization=a))
                return HttpResponseRedirect('http://localhost:8080/ui/' + str(list_id) + '/tasks/')
        else:
            form = TaskDetailsForm()
        resp = requests.get('http://127.0.0.1:8000/todolists/' + str(list_id) + '/tasks/',
                        headers = dict(Authorization=a)).json()
        task = ''
        for i in resp:
            if str(i['id']) + '/' == str(pk):
                task = i
        if task == '':
            raise Http404
        for i in ['name', 'description', 'completed', 'due_date', 'tags', 'priority']:
            form.fields[i].initial = task[i]
        form.fields['tags'].initial = ''
        for i in task['tags']:
            form.fields['tags'].initial += i + ' '
        return render(request, 'taskdetailstpl.html', {'form': form, 'action': request.path})

def tasklist_details(request, pk):
        try:
            a = request.session['Authorization']
        except:
            return HttpResponseRedirect('http://localhost:8080/ui/login/')
        if request.method == 'POST':
            form = TasklistDetailsForm(request.POST)
            if form.is_valid():
                method = form.cleaned_data['method']
            if method == 'put':
                if form.is_valid():
                    requests.Session().put('http://127.0.0.1:8000/todolists/' + str(pk) + '/', 
                                data = dict(name = form.cleaned_data['name']),
                                headers = dict(Authorization=a))
            else:
                requests.Session().delete('http://127.0.0.1:8000/todolists/' + str(pk) + '/',
                            headers = dict(Authorization=a))
                return HttpResponseRedirect('http://localhost:8080/ui/')
        else:
            form = TasklistDetailsForm()
        resp = requests.get('http://127.0.0.1:8000/todolists/',
                        headers = dict(Authorization=a)).json()
        list = ''
        for i in resp:
            if str(i['id']) == str(pk):
                list = i
        if list == '':
            raise Http404
        form.fields['name'].initial = list['name']
        return render(request, 'tasklistdetailstpl.html', {'form': form, 'action': request.path, 'list': list})

def tag_create(request):
    try:
        a = request.session['Authorization']
    except:
        return HttpResponseRedirect('http://localhost:8080/ui/login/')
    if request.method == 'POST':
        form = TagCreateForm(request.POST)
        form.fields['tasks'].choices = get_tasks(request)
        if form.is_valid():
            requests.Session().post('http://127.0.0.1:8000/todolists/tags',
                data = dict(name = form.cleaned_data['name'],
                            tasks = form.cleaned_data['tasks']),
                headers = dict(Authorization=a))
    else:
        form = TagCreateForm()
        form.fields['tasks'].choices = get_tasks(request)
    resp = requests.get('http://127.0.0.1:8000/todolists/tags',
                        headers = dict(Authorization=a)).json()
    return render(request, 'tagcreatetpl.html', {'form': form, 'action': request.path, 'resp': resp})

def user_confirm(request):
    return render(request, 'confirmtpl.html')

def user_activate(request, activation_key):
    resp = requests.get('http://127.0.0.1:8000/activate/{}'.format(activation_key))
    return HttpResponseRedirect('http://localhost:8080/ui/login')


def user_registration(request):
    try:
        a = request.session['Authorization']
        return HttpResponseRedirect('http://localhost:8080/ui/')
    except:
        if request.method == 'POST':
            form = RegisterForm(request.POST)
            if form.is_valid():
                username = form.cleaned_data['username']
                password = form.cleaned_data['password']
                email = form.cleaned_data['email']
                data = dict(username=username, password=password, email=email)
                response = requests.post('http://127.0.0.1:8000/todolists/registration', data)
                return HttpResponseRedirect('http://localhost:8080/ui/confirm/')
        else:
            form = RegisterForm()
        return render(request, 'regtpl.html', {'form': form})

def user_login(request):
    text = ''
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            data = dict(username=username, password=password)
            #c = requests.Session() 
            #url = 'http://127.0.0.1:8000/api-token-auth/' 
            #login_data = dict(username=username, password=password) 
            #req = c.post(url, data=login_data)

            #c.headers['Authorization'] = "Token " + str(req.json()['token'])
            #req = c.get('http://127.0.0.1:8000/todolists/')
            #print(req)
            response = requests.post('http://127.0.0.1:8000/api-token-auth/', data)
            if response.status_code == 200:
                #request.session['Token'] = str(response.json()['token'])
                request.session['Authorization'] = 'Token ' + str(response.json()['token'])
                requests.Session().headers = dict(Authorization=request.session['Authorization'])
                #resp = requests.get('http://127.0.0.1:8000/todolists/', headers=dict(Authorization=request.session['Authorization']))
                #resp = requests.get('http://127.0.0.1:8000/todolists', headers=dict(Authorization='Token ' + str(response.json()['token'])))
                #print(resp)
            else:
                text = 'Sorry, wrong username or password'
    else:
        form = LoginForm()
    return render(request, 'logintpl.html', {'form': form, 'text': text})

def user_logout(request):
    request.session.pop('Authorization')
    return HttpResponseRedirect('http://localhost:8080/ui/login/')
